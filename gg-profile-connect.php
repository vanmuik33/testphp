<?php
require_once 'connect.php';
require 'google-api-php-client/vendor/autoload.php';
require '_permission.php';

// Creating new google client instance
$client = new Google\Client();

// // Enter your Client ID
// $client->setClientId('1069631398216-7m0fnifaojd6epffmskdgbl87tt0tl1n.apps.googleusercontent.com');
// // Enter your Client Secrect
// $client->setClientSecret('5gChbK3_UYQ8s1x2gKzSOZdq');

// Setup Oauth2 config
$client->setAuthConfig('client_secret_1069631398216-7m0fnifaojd6epffmskdgbl87tt0tl1n.apps.googleusercontent.com.json');

// Enter the Redirect URL
$client->setRedirectUri('https://muinv.lahvui.xyz/testphp/gg-profile-connect.php');

// Adding those scopes which we want to get (email & profile Information)
$client->addScope("email");
$client->addScope("profile");

//Setup alert variable
$show_alert = '<script>$(".alert").removeClass("hidden");</script>';
$hide_alert = '<script>$(".alert").addClass("hidden");</script>';
$success = '<script>$(".alert").attr("class", "alert alert-success");</script>';


if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $access_token = $client->getAccessToken();
    $client->setAccessToken($access_token);
    $google_oauth = new Google_Service_Oauth2($client);
    $google_account_info = $google_oauth->userinfo->get();

    $userGoogleID = $google_account_info->id;
    $email = $google_account_info->email;
    $firstName = $google_account_info->familyName;
    $lastName = $google_account_info->givenName;
    $avatar = $google_account_info->picture;

    $userID = $_SESSION['userID'];

    // // Update user database
    $sql = "UPDATE USERS SET `USER_GG_ID` = $userGoogleID WHERE `USER_ID` = $userID";

    if ($result = mysqli_query($connect, $sql)) {
        header('location: /testphp/social.php');
    }
} else {
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
}
