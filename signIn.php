<?php
require_once 'vendor/autoload.php';
include_once 'googleLogin.php';
include('checkUser.php');
include_once('function.php');
$userID = checkLoginType();
if ($userID) {
    header('location: /testphp');
}
if (isset($_POST['type']) && $_POST['type'] == 'signIn') {
    // include('connect.php');
    $userName = $_POST['userName'];
    $passWord = $_POST['passWord'];
    $passWord = md5($passWord);
    $checkUser_SQL = "SELECT * FROM `USERS` WHERE ((`USER_NAME` = '$userName' OR `EMAIL` = '$userName') AND `IS_DELETE` != 1)";
    $checkUser = mysqli_query($connect, $checkUser_SQL);
    if (mysqli_num_rows($checkUser) == 0) {
        $response = array(
            'success' => false,
            'error'   => '<li>This username does not exist</li>'
        );
        echo json_encode($response);
        die();
    }
    $userDB = mysqli_fetch_array($checkUser);
    if ($userDB['INACTIVE'] == 0) {
        $response = array(
            'success' => false,
            'error'   => '<li>Please active your account.</li>'
        );
        echo json_encode($response);
        die();
    }
    if ($passWord != $userDB['PASSWORD']) {
        $response = array(
            'success' => false,
            'error'   => '<li>Password id invalid.</li>'
        );
        echo json_encode($response);
        die();
    }
    if ($userDB == '') {
        $response = array(
            'success' => false,
            'error' => '<li></li>'
        );
        echo json_encode($response);
        die();
    } else {
        $getRoles = mysqli_query($connect, 'SELECT * FROM `ROLES`');
        $userRolesIdArray = explode(',', $userDB['ROLES']);
        while ($row = mysqli_fetch_assoc($getRoles)) {
            $RolesDB[$row['ROLE_ID']] = $row['LABEL'];
        }
        foreach ($RolesDB as $key => $value) {
            $RolesIdArray[] = $key;
        }
        foreach ($userRolesIdArray as $value) {
            if (in_array($value, $RolesIdArray)) {
                $userRoles[] = $RolesDB[$value];
            } else $userRoles = 1;
        }
        if ($userDB['ADMIN'] == 1) {
            $_SESSION['userID'] = $userDB['USER_ID'];
            $_SESSION['isAdmin'] = TRUE;
            $_SESSION['userLastName'] = $userDB['LAST_NAME'];
            $_SESSION['permission'] = $userRoles;
            $response = array(
                'success' => true
            );
            echo json_encode($response);
            die();
        } else {
            $_SESSION['userID'] = $userDB['USER_ID'];
            $_SESSION['userLastName'] = $userDB['LAST_NAME'];
            $_SESSION['permission'] = $userRoles;
            $response = array(
                'success' => true
            );
            echo json_encode($response);
            die();
        }
    }
}
if (isset($_GET['type']) && $_GET['type'] == 'reCAPTCHA') {
    require_once 'vendor/autoload.php';
    $secret = '6LcMPuEZAAAAALvhu8-Vu56tZVFOT9Oiq3LFBaSk';
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
        ->verify($_GET['response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()) {
        echo 1;
        die();
    } else {
        echo 0;
        die();
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign in</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <?php include('bootstrap3.php') ?>
    <style>
        .notification {
            font-size: 20px;
            margin-top: 20px;
            padding-left: none !important;
        }
    </style>
</head>

<body>
    <?php
    include('navbar.php');
    // require_once 'vendor/autoload.php';
    $fb = new Facebook\Facebook([
        'app_id' => '692251158364236', // Replace {app-id} with your app id
        'app_secret' => 'b3bb658861b7dfaeea689ae422dfd2c7',
        'default_graph_version' => 'v3.2',
    ]);

    $helper = $fb->getRedirectLoginHelper();
    $permissions = ['email']; // Optional permissions
    $callbackUrl = htmlspecialchars('https://muinv.lahvui.xyz/testphp/fb-callback.php');
    $loginUrl = $helper->getLoginUrl($callbackUrl, $permissions);
    ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h1 class="text-primary">Sign In</h1>
                <form action="" method="POST">
                    <div class="form-group">
                        <label for="userID">User Name:</label>
                        <input type="text" class="form-control" id="userName" name="userName">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input type="password" class="form-control" id="pwd" name="pwd">
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="remember"> Remember me</label>
                    </div>
                    <div class="form-group">
                        <a href="signUp.php">Sign Up</a> | <a href="forgotpw.php">Forgot password</a>
                    </div>
                    <div class="g-recaptcha" data-sitekey="6LcMPuEZAAAAAH3IZTULCezRZHva6D9gaPQwqbAp"></div>
                    <hr>
                    <button type="button" class="btn btn-primary" name="signIn" id="BTNsignIn">Sign in</button>
                    <a class="btn btn-primary" href="<?php echo $loginUrl ?>">Log in with Facebook!</a>
                    <a class="btn btn-primary" href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/testphp/oauth2callback.php' ?>">Log in with Google!</a>
                </form>
                <ul id="error" style="display: none;" class="notification text-danger">
                    Failed
                </ul>
                <div class="alert alert-danger hidden">
                    <?php
                    if (!empty($_SESSION['notifi'])) {
                        echo ($_SESSION['notifi']);
                        unset($_SESSION['notifi']);
                    } else {
                        echo '<script>$(".alert").addClass("hidden");</script>';
                    }
                    ?>
                </div>

            </div>
        </div>
    </div>

    <script src="ajax/signin.js"></script>
</body>

</html>