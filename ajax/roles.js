$(document).ready(function () {
    $('button[name=editRoles]').on('click', function () {
        userType = $('input[name=userType]:checked').val();
        roles = [];
        userID = $('input[name=userID]').val();
        $('input[name="activeRoleCheckbox[]"]:checked').each(function(i){
            roles[i] = $(this).val();
        });
        $error = '';
        // Check error code here
        if ($error != '') {
            $content = $error;
            $('#notification').html($content);
        }
        else {
            $.ajax({
                type: 'get',
                data: {
                    type: 'editRoles',
                    userType: userType,
                    roles: roles,
                    user: userID
                },
                success: function (response) {
                    if (response == 1) {
                        window.location.href = "/testphp/admin/roles.php?page=1";
                    }
                    else {
                        $('#notification').html(response);
                    }
                }
            });
        }
    });
    $('input[value=Cancel]').on('click', function () {
        window.history.back();
    });
});