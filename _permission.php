<?php
function checkPermission($role)
{
    if (isset($_SESSION['userID']) == false && isset($_SESSION['userGoogleID']) == false && isset($_SESSION['userFacebookID']) == false) {
        header('location: /testphp/signIn.php');
    } else {
        if (isset($_SESSION['permission']) == true) {
            // $permission =  explode(',', $_SESSION['permission']);
            if ($role == '') {
                echo 'Access denied. <br>';
                echo "<a href='/testphp/'> Click để về lại trang chủ</a>";
                exit();
            } else {
                foreach ($role as $value) {
                    if (in_array($value, $_SESSION['permission']) == false) {
                        echo 'Access denied. <br>';
                        echo "<a href='/testphp/'> Click để về lại trang chủ</a>";
                        exit();
                    }
                }
            }
        }
    }
}

function getPermission($connect, $userID)
{
    $sql = "SELECT `ROLES` FROM `USERS` WHERE (`USER_ID` = $userID OR `USER_FB_ID` = $userID OR `USER_GG_ID` = $userID)";
    $userDB = mysqli_fetch_assoc(mysqli_query($connect,$sql));

    $getRoles = mysqli_query($connect, 'SELECT * FROM `ROLES`');

    $userRolesIdArray = explode(',', $userDB['ROLES']);

    while ($row = mysqli_fetch_assoc($getRoles)) {
        $RolesDB[$row['ROLE_ID']] = $row['LABEL'];
    }

    foreach ($RolesDB as $key => $value) {
        $RolesIdArray[] = $key;
    }

    foreach ($userRolesIdArray as $value) {
        if (in_array($value, $RolesIdArray)) {
            $userRoles[] = $RolesDB[$value];
        } else $userRoles = 1;
    }
    return $userRoles;
}
