<?php
if (session_id() === '')
    session_start();
function checkLoginType()
{
    $userID = false;
    if (isset($_SESSION['userID'])) $userID = $_SESSION['userID'];
    else if (isset($_SESSION['userFacebookID'])) $userID = $_SESSION['userFacebookID'];
    else if (isset($_SESSION['userGoogleID'])) $userID = $_SESSION['userGoogleID'];
    else $userID = false;
    return $userID;
}

class Redirect
{
    public function __construct($url = null)
    {
        if ($url) {
            echo '<script>location.href="' . $url . '";</script>';
        }
    }
}

