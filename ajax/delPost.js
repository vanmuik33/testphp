$(document).ready(function () {
    $('#BTNdelPost').on('click', function () {
        $postID = $('#postID').val();
        $pageID = $('input[name=pageID').val();
        $.ajax({
            url: 'delPost.php',
            type: 'get',
            data: {
                type: 'delPost',
                postID: $postID,
                // pageID: $pageID
            }, success: function (data) {
                $node = document.getElementsByTagName('tr')[1];
                $("tr").remove("#" + $postID);
                if (!$.contains(document.body, $node))
                    window.location.replace('/testphp/admin/posts.php?page=' + ($pageID - 1));
                $('#deleteEmployeeModal').modal('hide');
                $('#notification .alert').removeClass('hidden');
                $('#notification .alert').html(data);
            }, error: function () {
                $('#notification .alert').removeClass('hidden');
                $('#notification .alert').html('Đã có lỗi xảy ra, hãy thử lại sau.');
            }
        });
    });
    $('#delMulUser').on('click', function () {
        $pageID = $('input[name=pageID').val();
        $checkedArray = [];
        $('input[name="userCheckbox[]"]:checked').each(function (i) {
            $checkedArray[i] = $(this).val();
        });
        $.ajax({
            url: 'delUser.php',
            type: 'get',
            data: {
                type: 'delMulUser',
                list: $checkedArray
            },
            success: function (response) {
                if (response == 1) {
                    $node = document.getElementsByTagName('tr')[1];
                    $.each($checkedArray, function (i) {
                        $('tr').remove('#' + $checkedArray[i]);
                    });
                    $('#deleteMultipleEmployeeModal').modal('hide');
                    if (!$.contains(document.body, $node))
                        window.location.replace('/testphp/admin/listUser.php?page=' + ($pageID - 1));

                } else {
                    $('#notification').html(response);
                }
            }
        });
    });
});
