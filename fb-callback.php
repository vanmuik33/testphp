<?php
session_start();
require_once 'vendor/autoload.php';
require_once '_permission.php';
require_once 'connect.php';

//Setup alert variable
$show_alert = '<script>$(".alert").removeClass("hidden");</script>';
$hide_alert = '<script>$(".alert").addClass("hidden");</script>';
$success = '<script>$(".alert").attr("class", "alert alert-success");</script>';

$fb = new Facebook\Facebook([
  'app_id' => '692251158364236', // Replace {app-id} with your app id
  'app_secret' => 'b3bb658861b7dfaeea689ae422dfd2c7',
  'default_graph_version' => 'v3.2',
]);

$helper = $fb->getRedirectLoginHelper();

try {
  $accessToken = $helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (!isset($accessToken)) {
  if ($helper->getError()) {
    header('HTTP/1.0 401 Unauthorized');
    echo "Error: " . $helper->getError() . "\n";
    echo "Error Code: " . $helper->getErrorCode() . "\n";
    echo "Error Reason: " . $helper->getErrorReason() . "\n";
    echo "Error Description: " . $helper->getErrorDescription() . "\n";
  } else {
    header('HTTP/1.0 400 Bad Request');
    echo 'Bad request';
  }
  exit;
}

$fb->setDefaultAccessToken($accessToken->getValue());

try {
  $response = $fb->get('/me?fields=name,email,picture,first_name,last_name');
  $userNode = $response->getGraphUser();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}
// /////////////////////////////////////////=============================>>> Logged in

// echo 'Logged in as ' . $userNode->getId() . '<br>'; 
// echo 'Logged in as ' . $userNode->getName(). '<br>'; 
// echo 'Logged in as ' . $userNode->getFirstName(). '<br>'; 
// echo 'Logged in as ' . $userNode->getEmail(). '<br>'; 
// print_r($userNode->getPicture()['url']);

// echo '<h3>Access Token</h3>';
// var_dump($accessToken->getValue());

// The OAuth 2.0 client handler helps us manage access tokens
$oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);


// Get user's info 
$userFacebookID = $userNode->getId();
$email = $userNode->getEmail();
$firstName = $userNode->getFirstName();
$lastName = $userNode->getLastName();
$avatar = $userNode->getPicture()['url'];

//Check if the user is connected to facebook
$sql = "SELECT * FROM `USERS` WHERE (`USER_FB_ID` = $userFacebookID AND `PASSWORD` <> ' ')";
$row = mysqli_num_rows(mysqli_query($connect, $sql));

if ($row > 0) { //If the user has connected to facebook
  // Set login info
  $row = mysqli_fetch_assoc(mysqli_query($connect, $sql));
  $_SESSION['permission'] = getPermission($connect, $row['USER_ID']);
  $_SESSION['userID'] = $row['USER_ID'];
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
} else {
  // Add user into database
  $sql = "SELECT `USER_FB_ID` FROM `USERS` WHERE `USER_FB_ID` = $userFacebookID";
  if (mysqli_num_rows(mysqli_query($connect, $sql)) == 0) {
    $sql = "SELECT `EMAIL` FROM `USERS` WHERE `EMAIL` =" . "'$email'";
    if (mysqli_num_rows(mysqli_query($connect, $sql)) == 0) {
      $sql = "INSERT INTO USERS (`USER_FB_ID`,`FIRST_NAME`,`LAST_NAME`,`EMAIL`,`AVATAR`,`ROLES`,`GROUP_ID`)
    VALUE ('$userFacebookID','$firstName','$lastName','$email','$avatar','1,2','3')";
      if (mysqli_query($connect, $sql)) { //If add user success
        // Set login info
        $_SESSION['notifi'] = $success . 'Bạn đang đăng nhập bằng Facebook.';
        $_SESSION['permission'] = getPermission($connect, $userNode->getId());
        $_SESSION['fb_access_token'] = (string) $accessToken;
        $_SESSION['userFacebookID'] = $userNode->getId();
        // // User is logged in with a long-lived access token.
        // // You can redirect them to a members-only page.
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
      } else {
        $_SESSION['notifi'] = $show_alert . 'Thông tin facebook của bạn chưa được thêm vào CSDL. Thử đăng nhập bằng tài khoản hoặc email đã đăng ký.';
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
      }
    } else {
      $_SESSION['notifi'] = $show_alert . 'Email của bạn đã được dùng để đăng ký thành viên. Hãy đăng nhập bằng email bạn đã đăng ký.';
      $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp/signIn.php';
      header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
    }
  } else {
    $_SESSION['notifi'] = $success . 'Bạn đang đăng nhập bằng Facebook.';
    $_SESSION['permission'] = getPermission($connect, $userNode->getId());
    $_SESSION['fb_access_token'] = (string) $accessToken;
    $_SESSION['userFacebookID'] = $userNode->getId();
    // // User is logged in with a long-lived access token.
    // // You can redirect them to a members-only page.
    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  }
}



// $_SESSION['permission'] = getPermission($connect, $userNode->getId());

// $_SESSION['fb_access_token'] = (string) $accessToken;
// $_SESSION['userFacebookID'] = $userNode->getId();

// // User is logged in with a long-lived access token.
// // You can redirect them to a members-only page.
// header('Location: https://muinv.lahvui.xyz/testphp/');
