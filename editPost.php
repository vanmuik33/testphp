<?php
$roleAdmin = array('READ','EDIT');
include_once('function.php');
include_once('_permission.php');
$userID = checkLoginType();
checkPermission($roleAdmin);

// Các biến xử lý thông báo
$show_alert = '<script>$("#formEditPost .alert").removeClass("hidden");</script>';
$hide_alert = '<script>$("#formEditPost .alert").addClass("hidden");</script>';
$success = '<script>$("#formEditPost .alert").attr("class", "alert alert-success");</script>';

include_once('connect.php');
$sql_get_data_post = "SELECT * FROM POSTS WHERE ID_POST =" . $_GET['idPost'];
$data_post = mysqli_fetch_assoc(mysqli_query($connect, $sql_get_data_post));

// Kiểm tra xem có phải là tác giả hay không
if ($_SESSION['userID'] != $data_post['AUTHOR_ID']) {
    echo $show_alert . 'Bạn không phải là tác giả của bài viết này.';
    die();
} else {
    if (isset($_POST['action']) && $_POST['action'] == 'edit_post') {
        
        // Xử lý các giá trị
        $id_post = trim(htmlspecialchars(addslashes($_POST['id_post'])));
        $stt_edit_post = trim(htmlspecialchars(addslashes($_POST['stt_edit_post'])));
        $title_edit_post = trim(htmlspecialchars(addslashes($_POST['title_edit_post'])));
        $slug_edit_post = trim(htmlspecialchars(addslashes($_POST['slug_edit_post'])));
        $url_thumb_edit_post = trim(htmlspecialchars(addslashes($_POST['url_thumb_edit_post'])));
        $desc_edit_post = trim(htmlspecialchars(addslashes($_POST['desc_edit_post'])));
        $keywords_edit_post = trim(htmlspecialchars(addslashes($_POST['keywords_edit_post'])));
        $body_edit_post = trim(htmlspecialchars(addslashes($_POST['body_edit_post'])));

        // // Kiểm tra id bài viết
        $sql_check_id_post = "SELECT ID_POST FROM POSTS WHERE ID_POST = '$id_post'";
        // // Nếu các giá trị rỗng
        if ($stt_edit_post == '' || $title_edit_post == '' || $slug_edit_post == '' || $body_edit_post == '') {
            echo $show_alert . 'Vui lòng điền đầy đủ thông tin.';
        } else if (!mysqli_num_rows(mysqli_query($connect, $sql_check_id_post))) {
            echo $show_alert . 'Đã có lỗi xảy ra, vui lòng thử lại.';
        }
        // // Kiểm tra url ảnh
        else if ($url_thumb_edit_post != '' && filter_var($url_thumb_edit_post, FILTER_VALIDATE_URL) === false) {
            echo $show_alert . 'Vui lòng nhập url thumbnail hợp lệ.';
        } else {
            // Sửa bài viết
            $sql_edit_post = "UPDATE POSTS SET
                `STATUS` = '$stt_edit_post',
                `TITLE` = '$title_edit_post',
                `SLUG` = '$slug_edit_post',
                `THUMB` = '$url_thumb_edit_post',
                `DESCR` = '$desc_edit_post',
                `KEYWORDS` = '$keywords_edit_post',
                `BODY` = '$body_edit_post'
                WHERE `ID_POST` = '$id_post';
            ";
            $result = mysqli_query($connect, $sql_edit_post);
            if ($result) echo 1;
            mysqli_close($connect);
            echo $show_alert . $success . 'Chỉnh sửa bài viết thành công.';
            new Redirect($_DOMAIN . 'posts.php');
        }
    }
}




?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Post</title>
    <?php include('bootstrap3.php') ?>
    <script src="https://cdn.ckeditor.com/4.15.1/standard-all/ckeditor.js"></script>
</head>

<body>
    <?php include('navbar.php') ?>
    <div class="container">
        <p class="form-edit-post">
            <form method="POST" id="formEditPost" data-id="<?php echo $_GET['idPost'] ?>" onsubmit="return false;">
                <div class="form-group">
                    <label>Trạng thái bài viết</label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="stt_edit_post" value="1" <?php if ($data_post['STATUS'] == 1) echo "checked" ?>> Xuất bản
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="stt_edit_post" value="0" <?php if ($data_post['STATUS'] == 0) echo "checked" ?>> Ẩn
                    </label>
                </div>

                <div class="form-group">
                    <label>Tiêu đề bài viết</label>
                    <input type="text" class="form-control title" value="<?php echo $data_post['TITLE'] ?>" id="title_edit_post">
                </div>
                <div class="form-group">
                    <label>Slug bài viết</label>
                    <input type="text" class="form-control slug" value="<?php echo $data_post['SLUG'] ?>" id="slug_edit_post">
                </div>
                <div class="form-group">
                    <label>Url thumbnail</label>
                    <input type="text" class="form-control" value="<?php echo $data_post['THUMB'] ?>" id="url_thumb_edit_post">
                </div>
                <div class="form-group">
                    <label>Mô tả bài viết</label>
                    <textarea id="desc_edit_post" class="form-control"><?php echo $data_post['DESCR'] ?></textarea>
                </div>
                <div class="form-group">
                    <label>Từ khoá bài viết</label>
                    <input type="text" class="form-control" value="<?php echo $data_post['KEYWORDS'] ?>" id="keywords_edit_post">
                </div>
                <div class="form-group">
                    <label>Nội dung bài viết</label>
                    <textarea cols="10" id="editor1" name="editor1" rows="10" data-sample-short><?php echo $data_post['BODY'] ?></textarea>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                    <button type="cancel" name="cancel" class="btn btn-primary">Cancel</button>
                </div>
                <div class="alert alert-danger hidden"></div>
            </form>
        </p>
    </div>
    <script src="ajax/editPost.js"></script>
    <script src="ajax/cancel.js"></script>
    <script>
        CKEDITOR.replace('editor1', {
            extraPlugins: 'editorplaceholder',
            editorplaceholder: 'Start typing here...'
        });
    </script>
</body>

</html>