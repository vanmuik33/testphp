$(document).ready(function () {
    $('button[name=updateProfile]').on('click', function () {
        $userData = new FormData($('form[name=formUpdate]')[0]);
        $userData.append('type', 'updateUser');
        // for (var value of $userData.values()) {
        //     console.log(value);
        // }
        $.ajax({
            url: 'profile.php',
            type: 'post',
            processData: false,
            contentType: false,
            data: $userData,
            success: function (response) {
                if (response == 1) {
                    window.location.href = "/testphp/";
                }
                else {
                    $('#notification').html(response);
                }
            }
        });
    });
});