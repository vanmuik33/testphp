$(document).ready(function () {
    $('.editBTN').on('click', function (e) {
        var useID = $(this).data('user-id');
        $('#editEmployeeModal input[name=userID]').attr('value', useID);
    });
    $('#editEmployeeModal').on('show.bs.modal', function (e) {
        // userID = $('#editEmployeeModal input[name=userID]').val();
        $.ajax({
            url: 'editUser.php',
            type: 'get',
            dataType: 'json',
            data: {
                type: 'getUserDB',
                userID: $('#editEmployeeModal input[name=userID]').val()
            },
            success: function (response) {
                $('#editEmployeeModal input[name=firstName]').val(response.FIRST_NAME);
                $('#editEmployeeModal input[name=lastName]').val(response.LAST_NAME);
                $('#editEmployeeModal input[name=email]').val(response.EMAIL);
                $('#editEmployeeModal input[name=psw]').val(response.PASSWORD);
                $('#editEmployeeModal input[name=gender]').each(function () {
                    if ($(this).val() == response.GENDER) {
                        $(this).attr('checked', 'checked');
                    }
                });
                $('#editEmployeeModal input[name="language[]"]').each(function () {
                    if ($.inArray($(this).val(), response.LANGUAGE) != -1) {
                        $(this).attr('checked', 'checked');
                    }
                    else $(this).removeAttr('checked');
                });
            }
        });
    });
    $('#editEmployeeModal button[name=editUser]').on('click', function () {
        $error = '';
        $('#notification').html($error);
        if ($('#editEmployeeModal input[name=firstName]').val() == '') $error += "<li>First Name cannot be left blank</li>";
        if ($('#editEmployeeModal input[name=lastName]').val() == '') $error += "<li>Last Name cannot be left blank</li>";
        if ($('#editEmployeeModal input[name=email]').val() == '') $error += "<li>Email cannot be left blank</li>";
        if ($error != '') {
            $('#notification').html($error);
        }
        else {
            $userData = new FormData($('#editEmployeeModal form')[0]);
            $userData.append('type', 'editUser');
            // for (var value of $userData.values()) {
            //     console.log(value);
            // }
            $.ajax({
                url: 'editUser.php',
                type: 'post',
                processData: false,
                contentType: false,
                data: $userData,
                // data: {
                //     type: 'editUser'
                // },
                success: function (response) {
                    if (response == 1) {
                        $('#editEmployeeModal').modal('hide'); //Hide modal
                        $('#'+$userData.get('userID')+' td:nth-child(3)').html($userData.get('firstName') + ' ' + $userData.get('lastName')); //Select row with userID to be added to formdata before
                        $('#'+$userData.get('userID')+' td:nth-child(5)').html($userData.get('email'));
                        switch($userData.get('gender')) {
                            case '0': $('#'+$userData.get('userID')+' td:nth-child(6)').html('Male'); break;
                            case '1': $('#'+$userData.get('userID')+' td:nth-child(6)').html('Female'); break;
                            case '2': $('#'+$userData.get('userID')+' td:nth-child(6)').html('Other'); break;
                        };
                        $content = '<h1 class="text-success">Edit user Successed!</h1>';
                        $('#notification').html($content);
                        // console.log($userData.get('userID'));

                    }
                    else {
                        $('#editEmployeeModal').modal('hide');
                        $('#notification').html(response);
                    }
                }
            });
        }
    });

});