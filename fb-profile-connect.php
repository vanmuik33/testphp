<?php
session_start();
require_once 'vendor/autoload.php';
require_once '_permission.php';
$fb = new Facebook\Facebook([
    'app_id' => '692251158364236', // Replace {app-id} with your app id
    'app_secret' => 'b3bb658861b7dfaeea689ae422dfd2c7',
    'default_graph_version' => 'v3.2',
]);

$helper = $fb->getRedirectLoginHelper();

try {
    $accessToken = $helper->getAccessToken();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}

if (!isset($accessToken)) {
    if ($helper->getError()) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Error: " . $helper->getError() . "\n";
        echo "Error Code: " . $helper->getErrorCode() . "\n";
        echo "Error Reason: " . $helper->getErrorReason() . "\n";
        echo "Error Description: " . $helper->getErrorDescription() . "\n";
    } else {
        header('HTTP/1.0 400 Bad Request');
        echo 'Bad request';
    }
    exit;
}

$fb->setDefaultAccessToken($accessToken->getValue());

try {
    $response = $fb->get('/me?fields=name,email,picture,first_name,last_name');
    $userNode = $response->getGraphUser();
} catch (Facebook\Exceptions\FacebookResponseException $e) {
    // When Graph returns an error
    echo 'Graph returned an error: ' . $e->getMessage();
    exit;
} catch (Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
    echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
}
// The OAuth 2.0 client handler helps us manage access tokens
$oAuth2Client = $fb->getOAuth2Client();

// Get the access token metadata from /debug_token
$tokenMetadata = $oAuth2Client->debugToken($accessToken);


// Get user's info 
$userFacebookID = $userNode->getId();
$email = $userNode->getEmail();
$firstName = $userNode->getFirstName();
$lastName = $userNode->getLastName();
$avatar = $userNode->getPicture()['url'];

$userID = $_SESSION['userID'];

// // Update user database
include('connect.php');
$sql = "UPDATE USERS SET `USER_FB_ID` = $userFacebookID WHERE `USER_ID` = $userID";

if ($result = mysqli_query($connect,$sql)) {
    header('location: /testphp/social.php');
}