<?php include_once('../connect.php');
if (isset($_GET['userID']) == false) {
    echo '<li>Access denied</li>';
    echo '<a href="/testphp/signIn.php">Go Home</a>';
    exit;
}
$getUserDB_SQL = "SELECT `USER_NAME`,`USER_ID`,FIRST_NAME,LAST_NAME,EMAIL,USERS.ROLES,GROUP_NAME FROM `USERS` INNER JOIN `GROUP_USER` ON USERS.GROUP_ID = GROUP_USER.GROUP_ID WHERE `USER_ID` =" . $_GET['userID'];
$getUserDB = mysqli_query($connect, $getUserDB_SQL);
$userDB = mysqli_fetch_array($getUserDB);
$getRoleDB = mysqli_query($connect, "SELECT * FROM `ROLES`");
$getGroupRole = mysqli_query(($connect), "SELECT * FROM GROUP_USER");
while ($groupRole = mysqli_fetch_array($getGroupRole)) {
    $key = $groupRole['GROUP_NAME'];
    $value = explode(',', $groupRole['ROLES']);
    $roless[$key] =  $value; // Use to pass to javascript
}
if (isset($_GET['type']) && $_GET['type'] == 'editRoles') {
    $sql = "SELECT GROUP_ID FROM GROUP_USER WHERE GROUP_NAME = " . "'" . $_GET["userType"] . "'";
    $groupID = mysqli_fetch_assoc(mysqli_query($connect, $sql))['GROUP_ID'];
    $roles = implode(',', $_GET['roles']);
    if ($_GET['userType'] == 'ADMIN') {
        $sql = "UPDATE `USERS` SET `GROUP_ID` = $groupID , `ADMIN` = 1, `ROLES` = " . "'" . $roles .  "'" . " WHERE USER_ID =" . $_GET['user'];
    } else {
        $sql = "UPDATE `USERS` SET `GROUP_ID` = $groupID , `ADMIN` = 0, `ROLES` = " . "'" . $roles .  "'" . " WHERE USER_ID =" . $_GET['user'];
    }
    $result = mysqli_query($connect, $sql);
    if ($result == 1) {
        echo 1;
        exit;
    } else {
        echo 1;
        exit;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Role detail</title>
    <?php include('../bootstrap3.php') ?>
    <script src="../js/role-edit.js"></script>
</head>

<body>
    <?php include('../navbar.php') ?>
    <div class="container">
        <div>
            <!-- <input type="hidden" name="pageID" value="<?php echo $_GET['pageID'] ?>"> -->
            <h2 class="text-primary">User </h2>
            <div style="padding-left: 30px;">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th style="width: 200px;">User name</th>
                            <th style="width: 500px;">Name</th>
                            <th>Email</th>
                        </tr>

                    </thead>
                    <tbody>
                        <tr>
                            <td style="width: 200px;"><?php echo $userDB['USER_NAME'] ?></td>
                            <td style="width: 500px;"><?php echo $userDB["FIRST_NAME"] . ' ' . $userDB["LAST_NAME"]; ?></td>
                            <td><?php echo $userDB["EMAIL"]; ?></td>
                        </tr>
                        <input type="hidden" name="userID" value="<?php echo $_GET['userID'] ?>">
                    </tbody>
                </table>
                <form action="" name="userType">
                    <div class="form-group">
                        <label for=""><b>User type</b></label> <br>
                        <?php
                        foreach ($roless as $key => $value) {
                        ?>
                            <label class="radio-inline">
                                <input type="radio" name="userType" value="<?php echo $key ?>" <?php if ($userDB['GROUP_NAME'] == $key) echo "checked" ?>><?php echo $key ?>
                            </label>
                        <?php
                        }
                        ?>
                    </div>
                </form>
            </div>

        </div>
        <div>
            <h2 class="text-primary">Roles </h2>
            <form action="" name="rolesTBL" style="padding-left: 30px;">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th style="width: 20px;">#</th>
                            <th style="width: 100px;">LABEL</th>
                            <th style="width: 400px;">DESCRIPTION</th>
                            <th style="width: 50px; text-align:center">ACTIVE</th>
                        </tr>

                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($row = mysqli_fetch_array($getRoleDB)) {
                            if (in_array($row['ROLE_ID'], explode(',', $userDB['ROLES']))) { ?>
                                <tr id="<?php echo $row['ROLE_ID'] ?>">
                                    <td style="width: 20px;">
                                        <?php echo $i ?>
                                    </td>
                                    <td style="width: 100px;">
                                        <?php echo $row['LABEL'] ?>
                                    </td>
                                    <td style="width: 400px;">
                                        <?php echo $row['DETAIL'] ?>
                                    </td>
                                    <td style="width: 50px; text-align:center">
                                        <span class="activeCheckbox">
                                            <input type="checkbox" name="activeRoleCheckbox[]" value="<?php echo $row['ROLE_ID'] ?>" checked>
                                            <label for="activeRoleCheckbox"></label>
                                        </span>
                                    </td>
                                </tr>
                            <?php
                            } else {
                            ?>
                                <tr id="<?php echo $row['ROLE_ID'] ?>">
                                    <td style="width: 20px;">
                                        <?php echo $i ?>
                                    </td>
                                    <td style="width: 100px;">
                                        <?php echo $row['LABEL'] ?>
                                    </td>
                                    <td style="width: 400px;">
                                        <?php echo $row['DETAIL'] ?>
                                    </td>
                                    <td style="width: 50px; text-align:center">
                                        <span class="custom-checkbox">
                                            <input type="checkbox" name="activeRoleCheckbox[]" value="<?php echo $row['ROLE_ID'] ?>" disabled>
                                            <label for="activeRoleCheckbox"></label>
                                        </span>
                                    </td>
                                </tr>
                        <?php
                            }
                            $i++;
                        };
                        ?>
                    </tbody>
                </table>

                <input type="button" class="btn btn-default" value="Cancel">
                <button type="button" class="btn btn-success" id="btn-edit" name="editRoles">Update</button>
            </form>
            <div id="notification">

            </div>
        </div>

    </div>
    <script>
        $(document).ready(function() {
            var rolesArray = <?php echo json_encode($roless); ?>;
            $('input[name=userType').on('change', function() {
                val = $(this).val();
                checkbox = $('input[name="activeRoleCheckbox[]"]');
                for (key in rolesArray) {
                    if (key == val) {
                        checkbox.each(function() {
                            if (jQuery.inArray($(this).val(), rolesArray[key]) >= 0) {
                                this.checked = true;
                                $(this).removeAttr('disabled');
                            } else {
                                this.checked = false;
                                $(this).prop('disabled', 'true');
                            }
                        })
                        break;
                    }
                }
            });
        });
    </script>
    <script src="../ajax/roles.js"></script>
    <script>
        $(document).ready(function() {
            var checkbox = $('input[name="activeRoleCheckbox[]"]');
            $('input[name="activeRoleCheckbox[]"][value=99]').click(function() {
                if (this.checked) {
                    checkbox.each(function() {
                        this.checked = true;
                    });
                } else {
                    checkbox.each(function() {
                        this.checked = false;
                    });
                }
            });
            checkbox.click(function() {
                if (!this.checked) {
                    $('input[name="activeRoleCheckbox[]"][value=99]').prop("checked", false);
                }
            });
        });
    </script>
</body>

</html>