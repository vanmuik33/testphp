<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

if (isset($_POST['type']) && $_POST['type'] == 'forgot') {
    include('connect.php');
    $email = $_POST['email'];
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    $error = '';
    if (!$email) {
        $error .= "<p>Invalid email address, please type a valid email address</p>";
        die();
    } else {
        $sel_query = "SELECT * FROM `USERS` WHERE `EMAIL`='" . $email . "' ";
        $result = mysqli_query($connect, $sel_query);
        $row = mysqli_num_rows($result);
        if ($row == '') {
            $error .= "<p>No user is registered with this email address!</p>";
            die();
        }
    }
    if ($error != "") {
        echo '<div class="error">' . $error . '</div>';
        die();
    } else {
        $expFormat = mktime(
            date("H"),
            date("i"),
            date("s"),
            date("m"),
            date("d") + 1,
            date("Y")
        );
        $expDate = date("Y-m-d H:i:s", $expFormat);
        $key = md5($email);
        $addKey = substr(md5(uniqid(rand(), 1)), 3, 10);
        $key = $key . $addKey;
        // Insert Temp Table
        mysqli_query(
            $connect,
            "INSERT INTO `TOKEN_TEMP` (`EMAIL`, `KEY`, `expDATE`) VALUES ('" . $email . "', '" . $key . "', '" . $expDate . "');"
        );
        $output = '<p>Dear user,</p>';
        $output .= '<p>Please click on the following link to reset your password.</p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p><a href="http://muinv.lahvui.xyz/testphp/reset-password.php?key=' . $key . '&email=' . $email . '&action=reset" target="_blank">http://muinv.lahvui.xyz/testphp/reset-password.php?key=' . $key . '&email=' . $email . '&action=reset</a></p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p>Please be sure to copy the entire link into your browser. The link will expire after 1 day for security reason.</p>';
        $output .= '<p>If you did not request this forgotten password email, no action is needed, your password will not be reset. However, you may want to log into your account and change your security password as someone may have guessed it.</p>';
        $output .= '<p>Thanks,</p>';
        $output .= '<p>AllPHPTricks Team</p>';
        $body = $output;
        $subject = "Password Recovery - AllPHPTricks.com";
        $email_to = $email;
        require 'vendor/autoload.php';
        $fromserver = "noreply@yourwebsite.com";

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // SMTP::DEBUG_OFF = off (for production use)
        // SMTP::DEBUG_CLIENT = client messages
        // SMTP::DEBUG_SERVER = client and server messages
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;

        //Set the encryption mechanism to use - STARTTLS or SMTPS
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = 'vanmuik33@gmail.com';

        //Password to use for SMTP authentication
        $mail->Password = 'ghfjstqryylrltxg';

        $mail->IsHTML(true);
        $mail->From = "noreply@yourwebsite.com";
        $mail->FromName = "AllPHPTricks";
        $mail->Sender = $fromserver; // indicates ReturnPath header
        $mail->Subject = $subject;
        $mail->Body = $body;
        //Set who the message is to be sent to
        $mail->AddAddress($email_to);
        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            die();
        } else {
            // $_SESSION['registered'] = true;
            echo 1;
            die();
        }
    }
}

if (isset($_POST['forgot']) && (!empty($_POST['email']))) {
    $email = $_POST['email'];
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    $error = '';
    if (!$email) {
        $error .= "<p>Invalid email address, please type a valid email address</p>";
    } else {
        $sel_query = "SELECT * FROM `USERS` WHERE `EMAIL`='" . $email . "' ";
        $result = mysqli_query($connect, $sel_query);
        $row = mysqli_num_rows($result);
        if ($row == '') {
            $error .= "<p>No user is registered with this email address!</p>";
        }
    }
    if ($error != "") {
        echo '<div class="error">' . $error . '</div?';
    } else {
        $expFormat = mktime(
            date("H"),
            date("i"),
            date("s"),
            date("m"),
            date("d") + 1,
            date("Y")
        );
        $expDate = date("Y-m-d H:i:s", $expFormat);
        $key = md5($email);
        $addKey = substr(md5(uniqid(rand(), 1)), 3, 10);
        $key = $key . $addKey;
        // Insert Temp Table
        mysqli_query(
            $connect,
            "INSERT INTO `TOKEN_TEMP` (`EMAIL`, `KEY`, `expDATE`) VALUES ('" . $email . "', '" . $key . "', '" . $expDate . "');"
        );
        $output = '<p>Dear user,</p>';
        $output .= '<p>Please click on the following link to reset your password.</p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p><a href="http://muinv.lahvui.xyz/testphp/reset-password.php?key=' . $key . '&email=' . $email . '&action=reset" target="_blank">http://muinv.lahvui.xyz/testphp/reset-password.php?key=' . $key . '&email=' . $email . '&action=reset</a></p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p>Please be sure to copy the entire link into your browser. The link will expire after 1 day for security reason.</p>';
        $output .= '<p>If you did not request this forgotten password email, no action is needed, your password will not be reset. However, you may want to log into your account and change your security password as someone may have guessed it.</p>';
        $output .= '<p>Thanks,</p>';
        $output .= '<p>AllPHPTricks Team</p>';
        $body = $output;
        $subject = "Password Recovery - AllPHPTricks.com";
        $email_to = $email;
        require 'vendor/autoload.php';
        $fromserver = "noreply@yourwebsite.com";

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // SMTP::DEBUG_OFF = off (for production use)
        // SMTP::DEBUG_CLIENT = client messages
        // SMTP::DEBUG_SERVER = client and server messages
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;

        //Set the encryption mechanism to use - STARTTLS or SMTPS
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = 'vanmuik33@gmail.com';

        //Password to use for SMTP authentication
        $mail->Password = 'ghfjstqryylrltxg';

        $mail->IsHTML(true);
        $mail->From = "noreply@yourwebsite.com";
        $mail->FromName = "AllPHPTricks";
        $mail->Sender = $fromserver; // indicates ReturnPath header
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress($email_to);
        $result = $mail->send();
        if (!$result) {
            echo "Mailer Error: " . $mail->ErrorInfo;
        } else {
            echo " <br /><br />
                                    <div class='error'>
                                    <p>An email has been sent to you with instructions on how to reset your password.</p>
                                    </div><br /><br /><br />";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forgot Password</title>
    <?php include('bootstrap3.php') ?>
</head>

<body>
    <?php include('navbar.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <h1 class="text-primary">Forgot password</h1>
                <form action="" method="POST" name="forgot">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <p>You have account? <a href="signIn.php">Sign in</a></p>
                    </div>
                    <button type="button" class="btn btn-primary" name="forgot">Forgot password</button>
                </form>
                <div id="notification">

                </div>
            </div>
        </div>
    </div>
    <script src="ajax/forgot.js"></script>
</body>

</html>