<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

require 'vendor/autoload.php';
if (isset($_POST['type']) && $_POST['type'] == 'registerUser') {
    include('connect.php');
    // require 'vendor/autoload.php';

    $userName = $_POST['userName'];
    $passWord = ($_POST['psw']);
    $passWordRepeat = ($_POST['psw-repeat']);
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $email = $_POST['email'];
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    $gender = $_POST['gender'];
    if (!empty($_POST['language'])) $language = implode(',', $_POST['language']);
    else $language = '';
    $error = '';
    $sql_syntax = '';
    $result = '';
    $sql = "SELECT * FROM `USERS` WHERE `USER_NAME` = '$userName'";
    if (mysqli_num_rows(mysqli_query($connect, $sql)) > 0) {
        $error .= '<li>User Name existed</li>';
    }
    $regex = "/([a-z0-9_]+|[a-z0-9_]+\.[a-z0-9_]+)@(([a-z0-9]|[a-z0-9]+\.[a-z0-9]+)+\.([a-z]{2,4}))/i";
    if (!preg_match($regex, $email)) {
        $error .= '<li>Email address not valid</li>';
    }

    // if (mysqli_num_rows(mysqli_query($connect, "SELECT EMAIL FROM USERS WHERE EMAIL='$email'")) > 0) {
    //     $error .= '<li>Email existed</li>';
    // }
    $regex = "/^(?=.+[A-Z]).{8}$/";

    // if (!preg_match($regex, $passWord)) {
    //     $error .= '<li>Password tu 6 den 32 ky tu, can co 1 chu cai viet hoa</li>';
    // }
    if ($error != '') {
        echo $error;
        die();
    } else {
        $passWord = md5($_POST['psw']);
        $sql_syntax = "INSERT INTO USERS (`USER_NAME`,`PASSWORD`,`FIRST_NAME`,`LAST_NAME`,`EMAIL`,`GENDER`,`LANGUAGE`)
            VALUE ('$userName','$passWord','$firstName','$lastName','$email','$gender','$language')";
        if ($result = mysqli_query($connect, $sql_syntax)) {

            $expFormat = mktime(
                date("H"),
                date("i"),
                date("s"),
                date("m"),
                date("d") + 1,
                date("Y")
            );

            $expDate = date("Y-m-d H:i:s", $expFormat);
            $key = md5((2418 * 2) . $email);
            $addKey = substr(md5(uniqid(rand(), 1)), 3, 10);
            $key = $key . $addKey;
            // Insert Temp Table
            mysqli_query(
                $connect,
                "INSERT INTO `TOKEN_TEMP` (`EMAIL`, `KEY`, `expDATE`) VALUES ('" . $email . "', '" . $key . "', '" . $expDate . "');"
            );
            $output = '<p>Dear user,</p>';
            $output .= '<p>Please click on the following link to active your password.</p>';
            $output .= '<p>-------------------------------------------------------------</p>';
            $output .= '<p><a href="https://muinv.lahvui.xyz/testphp/activeUser.php?key=' . $key . '&email=' . $email . '&action=active" target="_blank">Activation.php?key=' . $key . '&email=' . $email . '&action=active</a></p>';
            $output .= '<p>-------------------------------------------------------------</p>';
            $output .= '<p>Please be sure to copy the entire link into your browser. The link will expire after 1 day for security reason.</p>';
            $output .= '<p>If you did not request this forgotten password email, no action is needed, your password will not be reset.</p>';
            $output .= '<p>Thanks,</p>';
            $output .= '<p>AllPHPTricks Team</p>';
            $body = $output;
            $subject = "Activation Account - AllPHPTricks.com";
            $email_to = $email;
            $fromserver = "noreply@yourwebsite.com";

            //Create a new PHPMailer instance
            $mail = new PHPMailer();
            //Tell PHPMailer to use SMTP
            $mail->isSMTP();
            //Enable SMTP debugging
            // SMTP::DEBUG_OFF = off (for production use)
            // SMTP::DEBUG_CLIENT = client messages
            // SMTP::DEBUG_SERVER = client and server messages
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;

            //Set the hostname of the mail server
            $mail->Host = 'smtp.gmail.com';

            //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
            $mail->Port = 587;

            //Set the encryption mechanism to use - STARTTLS or SMTPS
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

            //Whether to use SMTP authentication
            $mail->SMTPAuth = true;

            //Username to use for SMTP authentication - use full email address for gmail
            $mail->Username = 'vanmuik33@gmail.com';

            //Password to use for SMTP authentication
            $mail->Password = 'ghfjstqryylrltxg';


            //Set who the message is to be sent from
            $mail->setFrom('noreply@gmail.com', 'First Last');

            //Set an alternative reply-to address
            $mail->addReplyTo('vanmuk33@yahoo.com', 'Yahoo Mail');

            //Set who the message is to be sent to
            $mail->AddAddress($email_to);

            $mail->IsHTML(true);
            // $mail->Sender = $fromserver; // indicates ReturnPath header
            $mail->Subject = $subject;
            $mail->Body = $body;

            if (!$mail->Send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
                die();
            } else {
                $_SESSION['registered'] = true;
                echo 1;
                die();
            }
        }
        die();
    }
}
if (isset($_GET['type']) && $_GET['type'] == 'reCAPTCHA') {
    require_once 'vendor/autoload.php';
    $secret = '6LcMPuEZAAAAALvhu8-Vu56tZVFOT9Oiq3LFBaSk';
    $recaptcha = new \ReCaptcha\ReCaptcha($secret);
    $resp = $recaptcha->setExpectedHostname($_SERVER['SERVER_NAME'])
        ->verify($_GET['response'], $_SERVER['REMOTE_ADDR']);
    if ($resp->isSuccess()) {
        echo 1;
        die();
    }
    else {
        echo 0;
        die();
    }
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <?php include('bootstrap3.php') ?>
    <style>
        * {
            box-sizing: border-box;
        }

        /* Overwrite default styles of hr */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* Add a blue text color to links */
        a {
            color: dodgerblue;
        }
    </style>
</head>

<body>
    <?php include('navbar.php'); ?>
    <form action="register.php" id="formSignUp" name="formSignUp" class="form-group" method="POST">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1 class="text-primary">Sign Up</h1>
                    <p>Please fill in this form to create an account.</p>
                    <hr />
                    <div class="form-group">
                        <label for="userName"><b>User Name</b></label>
                        <input type="text" placeholder="User Name" name="userName" id="usertName" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label for="firstName"><b>First Name</b></label>
                        <input type="text" placeholder="First Name" name="firstName" id="firstName" class="form-control" required />
                    </div>
                    <div class="form-group">
                        <label for="lastName"><b>Last Name</b></label>
                        <input type="text" placeholder="Last Name" name="lastName" id="lastName" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label for="email"><b>Email</b></label>
                        <input type="text" placeholder="Enter Email" name="email" id="email" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label for="psw"><b>Password</b></label>
                        <input type="password" placeholder="Enter Password" name="psw" id="psw" class="form-control" required />
                    </div>

                    <div class="form-group">
                        <label for="psw-repeat"><b>Repeat Password</b></label>
                        <input type="password" placeholder="Repeat Password" name="psw-repeat" id="psw-repeat" class="form-control" required />
                    </div>


                    <div class="form-group">
                        <label for=""><b>Gender</b></label> <br>
                        <label class="radio-inline">
                            <input type="radio" name="gender" checked value="0">Male
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="1">Female
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="2">Other
                        </label>
                    </div>

                    <div class="form-group">
                        <label for=""><b>Language</b></label> <br>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="1">Tiếng Việt
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="2">English
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="3">français
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="4">русский
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="5">中文
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="language[]" value="100">Other
                        </label>
                    </div>
                    <div class="g-recaptcha" data-sitekey="6LcMPuEZAAAAAH3IZTULCezRZHva6D9gaPQwqbAp"></div>
                    <hr />
                    <p class="signin text-left">Already have an account? <a href="signIn.php">Sign in</a>.</p>

                    <p>
                        By creating an account you agree to our
                        <a href="#">Terms & Privacy</a>.
                    </p>

                    <button type="button" id="BTNsignUp" class="btn btn-primary" name="signUp">Sign up</button>
                    <button type="reset" class="btn btn-primary">Reset</button>
                    <button type="button" class="btn btn-primary" name="cancel">Cancel</button>
                </div>
            </div>
        </div>
    </form>
    <div class="container">
        <div id="notification" class="notif"></div>
    </div>
    <script src="ajax/signup.js"></script>
    <script src="ajax/cancel.js"></script>
</body>

</html>