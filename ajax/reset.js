$(document).ready(function () {
    $('input[name=reset]').on('click', function () {
        $error = '';
        if ($('input[name=pass1]').val() == '') $error += "<li>Password cannot be left blank</li>";
        if ($('input[name=pass2]').val() == '') $error += "<li>Password cannot be left blank</li>";
        if ($('input[name=pass1').val() != $('input[name=pass2').val()) $error += "<li>Password and repeat-password not match</li>";
        if ($error != '') {
            $content = $error;
            $('#notification').html($content);
        }
        else {
            $.ajax({
                url: 'reset-password.php',
                type: 'post',
                data: {
                    type: 'reset',
                    pass1: $('input[name=pass1]').val(),
                    pass2: $('input[name=pass2]').val(),
                    email: $('input[name=email').val()
                },
                success: function (response) {
                    if (response == 1) {
                        $('form[name=update]').hide();
                        $content = '<p class="text-success">Congratulations! Your password has been updated successfully.</p><a href="/testphp/">Về trang chủ</a>';
                        $('#notification').html($content);
                    }
                    else {
                        $('#notification').html(response);
                    }
                }
            });
        }
    });
});