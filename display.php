<?php
include 'connect.php';
$_DOMAIN = 'https://muinv.lahvui.xyz/testphp/';
$idpost = $_GET['idpost'];
// Get post data
$sql = "SELECT * FROM `POSTS` WHERE `ID_POST` = '$idpost'";
$postData = mysqli_fetch_assoc(mysqli_query($connect, $sql));

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $postData['TITLE'] ?></title>
    <?php include 'bootstrap3.php' ?>
    <style>
    </style>
</head>

<body>
    <?php include 'navbar.php' ?>
    <div class="container">
        <div class="text-center">
            <h1 class="text-center"><?php echo $postData['TITLE'] ?></h1>
            <img src="<?php echo $postData['THUMB'] ?>" alt="<?php echo $postData['TITLE'] ?>" class="center">
            <p><?php echo htmlspecialchars_decode($postData['BODY']) ?></p>
        </div>
        <div>
            <h4>Bài viết liên quan</h4>
            <?php
            $sql = "SELECT * FROM `POSTS` WHERE (`KEYWORDS` = '" . $postData['KEYWORDS']."' AND `ID_POST` <>  " . $postData['ID_POST'].")" ;
            $getPost = mysqli_query($connect, $sql);
            while ($row = mysqli_fetch_assoc($getPost)) {
            ?>
                <div class="col-sm-2">
                    <div class="thumbnail">
                        <a href="<?php echo $_DOMAIN . $row['SLUG'] . '-' . $row['ID_POST'] . '.html' ?>"> <img class="thumb" src="<?php echo $row['THUMB'] ?>" alt="<?php echo $row['KEYWORDS'] ?>"></a>
                        <div class="caption">
                            <h3><a href="<?php echo $_DOMAIN . 'display.php?idpost=' . $row['ID_POST'] ?>"><?php echo $row['TITLE'] ?></a></h3>
                            <?php echo htmlspecialchars_decode($row['DESCR']) ?>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
</body>

</html>