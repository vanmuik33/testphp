<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

if (isset($_POST['type']) && $_POST['type'] == 'reactive') {
    include_once('connect.php');
    require 'vendor/autoload.php';
    $email = $_POST['email'];
    $checkUser_SQL = "SELECT `INACTIVE` FROM `USERS` WHERE `EMAIL` = ' . $email '";
    $checkUser = mysqli_query($connect, $checkUser_SQL);
    $userDB = mysqli_fetch_assoc($checkUser);
    if ($userDB['ISACTIVE'] == 1) {
        echo 'This user is already activated';
        die();
    } else {
        $expFormat = mktime(
            date("H"),
            date("i"),
            date("s"),
            date("m"),
            date("d") + 1,
            date("Y")
        );

        $expDate = date("Y-m-d H:i:s", $expFormat);
        $key = md5((2418 * 2) . $email);
        $addKey = substr(md5(uniqid(rand(), 1)), 3, 10);
        $key = $key . $addKey;
        // Insert Temp Table
        mysqli_query(
            $connect,
            "INSERT INTO `TOKEN_TEMP` (`EMAIL`, `KEY`, `expDATE`) VALUES ('" . $email . "', '" . $key . "', '" . $expDate . "');"
        );
        $output = '<p>Dear user,</p>';
        $output .= '<p>Please click on the following link to active your password.</p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p><a href="https://muinv.lahvui.xyz/testphp/activeUser.php?key=' . $key . '&email=' . $email . '&action=active" target="_blank">Activation.php?key=' . $key . '&email=' . $email . '&action=active</a></p>';
        $output .= '<p>-------------------------------------------------------------</p>';
        $output .= '<p>Please be sure to copy the entire link into your browser. The link will expire after 1 day for security reason.</p>';
        $output .= '<p>If you did not request this forgotten password email, no action is needed, your password will not be reset.</p>';
        $output .= '<p>Thanks,</p>';
        $output .= '<p>AllPHPTricks Team</p>';
        $body = $output;
        $subject = "Activation Account - AllPHPTricks.com";
        $email_to = $email;
        $fromserver = "noreply@yourwebsite.com";

        //Create a new PHPMailer instance
        $mail = new PHPMailer();
        //Tell PHPMailer to use SMTP
        $mail->isSMTP();
        //Enable SMTP debugging
        // SMTP::DEBUG_OFF = off (for production use)
        // SMTP::DEBUG_CLIENT = client messages
        // SMTP::DEBUG_SERVER = client and server messages
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;

        //Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;

        //Set the encryption mechanism to use - STARTTLS or SMTPS
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        //Whether to use SMTP authentication
        $mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = 'vanmuik33@gmail.com';

        //Password to use for SMTP authentication
        $mail->Password = 'ghfjstqryylrltxg';


        //Set who the message is to be sent from
        $mail->setFrom('noreply@gmail.com', 'First Last');

        //Set an alternative reply-to address
        $mail->addReplyTo('vanmuk33@yahoo.com', 'Yahoo Mail');

        //Set who the message is to be sent to
        $mail->AddAddress($email_to);

        $mail->IsHTML(true);
        // $mail->Sender = $fromserver; // indicates ReturnPath header
        $mail->Subject = $subject;
        $mail->Body = $body;

        if (!$mail->Send()) {
            echo "Mailer Error: " . $mail->ErrorInfo;
            die();
        } else {
            $_SESSION['registered'] = true;
            echo 1;
            die();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>
    <?php include('bootstrap3.php'); ?>
</head>

<body>
    <?php include('navbar.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <form action="" method="POST" id="reActiveForm">
                    <div class="form-group">
                        <h4 class="text-primary">Please input your email to active your account!</h4>
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" value="<?php echo ($_GET['email']) ?>">
                    </div>
                    <button type="button" class="btn btn-primary" name="reActive" id="BTNactive">Active</button>
                </form>
                <div id="notification">

                </div>
            </div>
        </div>
    </div>
    <script src="ajax/reactive.js"></script>
</body>

</html>