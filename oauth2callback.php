<?php
require_once 'connect.php';
require 'google-api-php-client/vendor/autoload.php';
require 'googleLogin.php';
require '_permission.php';

//Setup alert variable
$show_alert = '<script>$(".alert").removeClass("hidden");</script>';
$hide_alert = '<script>$(".alert").addClass("hidden");</script>';
$success = '<script>$(".alert").attr("class", "alert alert-success");</script>';

if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $access_token = $client->getAccessToken();
    $client->setAccessToken($access_token);
    $google_oauth = new Google_Service_Oauth2($client);
    $google_account_info = $google_oauth->userinfo->get();

    $userGoogleID = $google_account_info->id;
    $email = $google_account_info->email;
    $firstName = $google_account_info->familyName;
    $lastName = $google_account_info->givenName;
    $avatar = $google_account_info->picture;

    //Check if the user is connected to Google
    $sql = "SELECT * FROM `USERS` WHERE (`USER_GG_ID` = $userGoogleID AND `PASSWORD` <> ' ')";
    $row = mysqli_num_rows(mysqli_query($connect, $sql));

    if ($row > 0) { //If the user has connected to Google
        $row = mysqli_fetch_assoc(mysqli_query($connect, $sql));
        $_SESSION['permission'] = getPermission($connect, $row['USER_ID']);
        $_SESSION['userID'] = $row['USER_ID'];
        echo $_SESSION['userID'];
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
    } else { //If user isn't connect to Google
        $sql = "SELECT `USER_GG_ID` FROM `USERS` WHERE `USER_GG_ID` = '$userGoogleID'";         // check google id is existed
        if (mysqli_num_rows(mysqli_query($connect, $sql)) == 0) { //If google id isn't exist
            $sql = "SELECT `EMAIL` FROM `USERS` WHERE `EMAIL` =" . "'$email'"; //check google email is existed
            if (mysqli_num_rows(mysqli_query($connect, $sql)) == 0) { //If google email isn't exist
                $sql = "INSERT INTO USERS (`USER_GG_ID`,`FIRST_NAME`,`LAST_NAME`,`EMAIL`,`AVATAR`,`ROLES`,`GROUP_ID`)
            VALUE ('$userGoogleID','$firstName','$lastName','$email','$avatar','1,2','3')"; //Add google info into database
                if (mysqli_query($connect, $sql)) { //If add user success
                    // Set login info
                    $_SESSION['notifi'] = $success . 'Bạn đang đăng nhập bằng Googleeee.';
                    $_SESSION['permission'] = getPermission($connect, $userGoogleID);
                    $_SESSION['google_access_token'] = $access_token;
                    $_SESSION['userGoogleID'] = $userGoogleID;

                    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
                    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
                } else {
                    $_SESSION['notifi'] = $show_alert . 'Thông tin google của bạn chưa được thêm vào CSDL.';
                    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
                    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
                }
            } else {
                $_SESSION['notifi'] = $show_alert . 'Email của bạn đã được dùng để đăng ký thành viên. Hãy đăng nhập bằng email bạn đã đăng ký.';
                $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp/signIn.php';
                header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
            }
        } else {
            $_SESSION['notifi'] = $success . 'Bạn đang đăng nhập bằng Googlefff.';
            $_SESSION['permission'] = getPermission($connect, $userGoogleID);
            $_SESSION['google_access_token'] = $access_token;
            $_SESSION['userGoogleID'] = $userGoogleID;

            $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/testphp';
            header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
        }
    }
} else {
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
}
