<?php
include ('connect.php');
include_once ('function.php');
include ('_permission.php');
global $result;
$userID = checkLoginType();
$getUserDB_SQL = "SELECT * FROM `USERS` WHERE (`USER_ID` = '$userID' OR `USER_FB_ID` = '$userID' OR `USER_GG_ID` = '$userID')";
$getUserDB = mysqli_query($connect, $getUserDB_SQL);
$userDB = mysqli_fetch_array($getUserDB);
$email = $userDB['EMAIL'];
$userLanguage = explode(',', $userDB['LANGUAGE']);
$linkAvatar = $userDB['AVATAR'];
$getLanguageTable_SQL = "SELECT `ID`,`LANGUAGE` FROM `LANGUAGE`";
$getLanguageTable = mysqli_query($connect, $getLanguageTable_SQL);
$error = '';
if (isset($_POST['type']) && $_POST['type'] == 'updateUser') {
    if (mysqli_num_rows(mysqli_query($connect, "SELECT `EMAIL`,`USER_ID` FROM USERS WHERE (`EMAIL` = '" . $_POST['email'] . "' AND `USER_ID` != $userID AND `USER_GG_ID` != $userID AND `USER_FB_ID` != '$userID')")) > 0) {
        $error .= '<li>Email existed</li>';
    }
    // if (mysqli_num_rows(mysqli_query($connect, "SELECT `EMAIL`,`USER_ID` FROM USERS WHERE (`EMAIL` = " . $_POST['email'] . " AND `USER_ID` != $userID)")) > 0) {
    //     $error .= '<li>Email existed</li>';
    // }
    if ($error != '') {
        echo $error;
        die();
        } else {
            if ($_FILES['avatar']['name'] != '') {
                // Setup folder to save upload file
                $target_dir = 'avatar/';
                $target_file   = $target_dir . basename($_FILES["avatar"]["name"]) . $_POST['email'];
                $allowUpload   = true;
                //Lấy phần mở rộng của file (jpg, png, ...)
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

                // Cỡ lớn nhất được upload (bytes)
                $maxfilesize   = 800000;

                ////Những loại file được phép upload
                $allowtypes    = array('jpg', 'png', 'jpeg', 'gif');
                move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file);
                $linkAvatar = '/testphp/' . $target_file;
                // 'https://' . $_SERVER['SERVER_NAME'] . 
            }
            $firstName = $_POST['firstName'];
            $lastName = $_POST['lastName'];
            $email = $_POST['email'];
            $gender = $_POST['gender'];
            if (!empty($_POST['language'])) $language = implode(',', $_POST['language']);
            else $language = '';
            $updateSQL_Syntax = "UPDATE `USERS` SET `FIRST_NAME` = '$firstName', `LAST_NAME` = '$lastName', `EMAIL` = '$email', `GENDER` = '$gender', `AVATAR` = '$linkAvatar', `LANGUAGE` = '$language' WHERE `USER_ID` = '$userID'";
            $result = mysqli_query($connect, $updateSQL_Syntax);
            if ($result) {
                $_SESSION['updated'] = true;
                echo 1;
                die();
            } else {
                echo 0;
                die();
            }
    }
};


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile</title>
    <?php include('bootstrap3.php') ?>
    <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/font-awesome-line-awesome/css/all.min.css">
    <style>
        .social {
            width: 100px;
            text-align: center;
            margin-top: 5px;
            margin-right: 20px;
        }
    </style>
</head>

<body>
    <?php
    include('navbar.php');
    ?>
    <form action="" name="formUpdate" enctype="multipart/form-data" class="form-group" method="POST">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1 class="text-primary">Your Profile</h1>
                    <p>Please fill in this form to update your profile.</p>
                    <hr />
                    <div class="form-group">
                        <label for="firstName"><b>First Name</b></label>
                        <input type="text" placeholder="First Name" name="firstName" id="firstName" class="form-control" value="<?php echo $userDB['FIRST_NAME'] ?>" require />
                    </div>
                    <div class="form-group">
                        <label for="lastName"><b>Last Name</b></label>
                        <input type="text" placeholder="Last Name" name="lastName" id="lastName" class="form-control" value="<?php echo $userDB['LAST_NAME'] ?>" required />
                    </div>

                    <div class="form-group">
                        <label for="email"><b>Email</b></label>
                        <input type="text" placeholder="Enter Email" name="email" id="email" class="form-control" value="<?php echo $email ?>" required />
                    </div>
                    <div class="form-group">
                        <label for=""><b>Gender</b></label> <br>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="0" <?php if ($userDB['GENDER'] == 0) echo "checked" ?>>Male
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="1" <?php if ($userDB['GENDER'] == 1) echo "checked" ?>>Female
                        </label>
                        <label class="radio-inline">
                            <input type="radio" name="gender" value="2" <?php if ($userDB['GENDER'] == 2) echo "checked" ?>>Other
                        </label>
                    </div>
                    <div class="form-group">
                        <label for="avatar"><b>Avatar</b></label>
                        <input type="file" name="avatar" id="avatar" class="form-control" require />
                    </div>
                    <div class="form-group">
                        <label for=""><b>Language</b></label> <br>
                        <?php
                        while ($languageDB = mysqli_fetch_assoc($getLanguageTable)) {
                            if (in_array($languageDB['ID'], $userLanguage)) { ?>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="language[]" value="<?php echo $languageDB['ID'] ?>" checked><?php echo $languageDB['LANGUAGE'] ?>
                                </label>
                            <?php
                            } else { ?>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="language[]" value="<?php echo $languageDB['ID'] ?>"><?php echo $languageDB['LANGUAGE'] ?>
                                </label>
                        <?php
                            }
                        } ?>
                    </div>
                    <div class="form-group">
                        <label for=""><b>Social Network</b></label><br>
                        <a href="social.php">Connect</a>
                    </div>
                    <div id="results"></div>
                    <hr />
                    <button type="button" class="btn btn-primary" name="updateProfile">Update</button>
                    <button type="button" class="btn btn-primary" name="cancel">Cancel</button>

                </div>
            </div>
        </div>
    </form>
    <div id="notification">
    </div>
    <script src="ajax/cancel.js"></script>
    <script src="ajax/update.js"></script>
</body>

</html>