<?php
include 'connect.php';
require_once 'vendor/autoload.php';

//Setup alert variable
$show_alert = '<script>$(".alert").removeClass("hidden");</script>';
$hide_alert = '<script>$(".alert").addClass("hidden");</script>';
$success = '<script>$(".alert").attr("class", "alert alert-success");</script>';

if (isset($_GET['fbID'])) {
    $userFacebookID = $_GET['fbID'];
    $sql = "UPDATE `USERS` SET `USER_FB_ID` = '' WHERE `USER_FB_ID` = $userFacebookID";
    $result = mysqli_query($connect, $sql);
    if ($result) {
        $_SESSION['notifi'] = $success . 'Huỷ kết nối facebook thành công';
        header('location: /testphp/social.php');
        die();
    }
}

if (isset($_GET['ggID'])) {
    $userGoogleID = $_GET['ggID'];
    $sql = "UPDATE `USERS` SET `USER_GG_ID` = '' WHERE `USER_GG_ID` = $userGoogleID";
    $result = mysqli_query($connect, $sql);
    if ($result) {
        $_SESSION['notifi'] = $success . 'Huỷ kết nối Google thành công';
        $GLOBALS['notifi'] = $success . 'Huỷ kết nối Google thành công';
        header('location: /testphp/social.php');
        die();
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Social Network</title>
    <?php include 'bootstrap3.php' ?>
    <style>
        .social {
            width: 150px;
            height: 50px;
            margin-right: 20px;
            margin-top: 10px;
            background: #337ab7;
            color: #fff;
            text-align: center;
            vertical-align: middle;
            line-height: 50px;
            display: inline-block;
        }
    </style>
</head>
<?php
include 'navbar.php';
$fb = new Facebook\Facebook([
    'app_id' => '692251158364236', // Replace {app-id} with your app id
    'app_secret' => 'b3bb658861b7dfaeea689ae422dfd2c7',
    'default_graph_version' => 'v3.2',
]);

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email']; // Optional permissions
$callbackUrl = htmlspecialchars('https://muinv.lahvui.xyz/testphp/fb-profile-connect.php');
$loginUrl = $helper->getLoginUrl($callbackUrl, $permissions);

//Check if the user is connected to facebook
$sql = "SELECT `USER_FB_ID`,`USER_GG_ID` FROM `USERS` WHERE `USER_ID` = " . $_SESSION['userID'];
$row = mysqli_fetch_assoc(mysqli_query($connect, $sql));


?>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <?php if ($row['USER_FB_ID'] == '') { ?>
                <div class="social">Facebook</div><a href="<?php echo $loginUrl ?>">Connect</a><br>
            <?php } else { ?>
                <div class="social">Facebook</div><a id="fb-dis" href="?fbID=<?php echo $row['USER_FB_ID'] ?>">Disconnect</a><br>
            <?php }; ?>
            <?php if ($row['USER_GG_ID'] == '') { ?>
                <div class="social">Google</div><a href="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . '/testphp/gg-profile-connect.php' ?>">Connect</a><br>
            <?php } else { ?>
                <div class="social">Google</div><a id="gg-dis" href="?ggID=<?php echo $row['USER_GG_ID'] ?>">Disconnect</a><br>
            <?php }; ?>

            <div class="alert alert-danger hidden">
                <?php
                if (!empty($_SESSION['notifi'])) {
                    echo($_SESSION['notifi']);
                    unset($_SESSION['notifi']);
                } else {
                    echo '<script>$(".alert").addClass("hidden");</script>';
                }
                
                ?>
            </div>
        </div>
    </div>

</div>
<!-- <script src="ajax/disSocial.js"></script> -->
</body>

</html>