<?php
include_once('connect.php');
include_once('function.php');

// if (isset($_SESSION['isAdmin'])) {
//     header('location: admin');
//     exit;
// }

$limit = 6;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$from = ($page - 1) * $limit;
$getDB_SQL = "SELECT * FROM `POSTS` WHERE `STATUS` = 0 ORDER BY `ID_POST` LIMIT $from , $limit";
$DB = mysqli_query($connect, $getDB_SQL);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home</title>
    <?php include('bootstrap3.php'); ?>
    <style>
        .thumbnail {
            height: 350px;
        }

        .thumb {
            height: 200px !important;
            width: 100%;
        }
    </style>
</head>

<body>

    <?php include('navbar.php'); ?>
    <div class="container">
        <div class="alert alert-danger hidden"><?php if (isset($_SESSION['notifi'])) echo $_SESSION['notifi']; ?></div>
        <?php
        unset($_SESSION['notifi']);
        if (!$userID = checkLoginType()) echo '<h1 class="text-primary">HOMEPAGE</h1>';
        else {
            $userDB_sql = "SELECT `LAST_NAME` FROM `USERS` WHERE (`USER_ID` = '$userID' OR `USER_FB_ID` = '$userID' OR `USER_GG_ID` = '$userID')";
            $getUserDB = mysqli_query($connect, $userDB_sql);
            $userDB = mysqli_fetch_assoc($getUserDB);
            echo '<h2 class="text-success">Welcome ' . $userDB['LAST_NAME'] . ' xinh đẹp.!!!</h2>';
        }
        if (isset($_SESSION['updated'])) {
            echo '<h2 class="text-success"> Update profile successfully </h2>';
            unset($_SESSION['updated']);
        }
        include_once('last-news.php');
        ?>
        <?php
        $getDBcount = mysqli_query($connect, "SELECT COUNT(`ID_POST`) FROM `POSTS` WHERE `STATUS` = 0 ");
        $DBcout = mysqli_fetch_row($getDBcount);
        $totalUser = $DBcout[0];
        $totalPage = ceil($totalUser / $limit);
        $pagLink = "<ul class='pagination pull-right'>";
        if ($page < $totalPage && $totalPage > 1) {
            $pagLink .= "<li class='page-item'><a class='page-link' href='index.php?page=" . ($page - 1) . "'><span class='glyphicon glyphicon-chevron-left'></span></a></li>";
        }
        for ($i = 1; $i <= $totalPage; $i++) {
            if ($page == $i) $active = 'active';
            else $active = '';
            $pagLink .= "<li class='page-item " . $active . "'><a class='page-link' href='index.php?page=" . $i . "'>" . $i . "</a></li>";
        }
        if ($page < $totalPage && $totalPage > 1) {
            $pagLink .= "<li class='page-item'><a class='page-link' href='index.php?page=" . ($page + 1) . "'><span class='glyphicon glyphicon-chevron-right'></span></a></li>";
        }
        echo $pagLink . "</ul>";

        ?>
    </div>
</body>

</html>