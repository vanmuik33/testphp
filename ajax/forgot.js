$(document).ready(function(){
    $('button[name=forgot]').on('click',function(){
        var email = $('#email').val();
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        if (!pattern.test(email)) {
            $content = '<h4 class="text-danger">Not a valid email</h4>';
            $('#notification').html($content);
        }
        else {
            $.ajax({
                url: 'forgotpw.php',
                type: 'post',
                data: {
                    type: 'forgot',
                    email: email
                },
                success: function (response) {
                    if (response == 1) {
                        $('form[name=forgot]').hide();
                        $content = '<h1 class="text-success">Successed!</h1><p class="decs"> The reset password mail is sent to your email. Click the activation link to change your password.</p><a href="/testphp/">Về trang chủ</a>';
                        $('#notification').html($content);
                    }
                    else {
                        console.log('flase');
                    }
                }
            });
        }
    });
});