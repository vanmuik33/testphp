<?php
$roleAdmin = array('FULL ACCESS');
require_once('../connect.php');
include('../_permission.php');
checkPermission($roleAdmin);
$limit = 5;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$from = ($page - 1) * $limit;
$getDB_SQL = "SELECT * FROM `USERS` WHERE `IS_DELETE` !=1 ORDER BY `USER_ID` LIMIT $from , $limit";
$getDB = mysqli_query($connect, $getDB_SQL);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List User</title>
    <?php include('../bootstrap3.php') ?>
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/fillData.js" type="text/javascript"></script>
</head>

<body>
    <?php include('../navbar.php') ?>
    <div class="container">
        <p id="success"></p>
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Manage <b>Users</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><span class="glyphicon glyphicon-plus-sign"> </span><span> Add New User</span></a>
                        <a href="#deleteMultipleEmployeeModal" class="btn btn-danger" id="delete_multiple" data-toggle="modal"><span class="glyphicon glyphicon-minus-sign"> </span> <span> Delete</span></a>
                        <a href="roles.php?page=1" class="btn btn-info" id="e" ><span class="glyphicon glyphicon-check"> </span> <span> Roles & Permissions</span></a>
                    </div>
                </div>
                <?php
                $getDBcount = mysqli_query($connect, "SELECT COUNT(`USER_ID`) FROM `USERS` WHERE `IS_DELETE` !=1 ");
                $DBcout = mysqli_fetch_row($getDBcount);
                $totalUser = $DBcout[0];
                $totalPage = ceil($totalUser / $limit);
                $pagLink = "<ul class='pagination pull-right'>";
                for ($i = 1; $i <= $totalPage; $i++) {
                    if ($page == $i) $active = 'active';
                    else $active = '';
                    $pagLink .= "<li class='page-item " . $active . "'><a class='page-link' href='listUser.php?page=" . $i . "'>" . $i . "</a></li>";
                }
                echo $pagLink . "</ul>";
                ?>
            </div>
            <form action="" id="userForm">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="selectAll" name="selectAll">
                                    <label for="selectAll"></label>
                                </span>
                            </th>
                            <th>#</th>
                            <th>NAME</th>
                            <th>ADMIN</th>
                            <th>EMAIL</th>
                            <th>GENDER</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($row = mysqli_fetch_array($getDB)) {
                        ?>
                            <tr id="<?php echo $row["USER_ID"]; ?>">
                                <td>
                                    <span class="custom-checkbox">
                                        <input type="checkbox" name="userCheckbox[]" value="<?php echo $row["USER_ID"]; ?>" class="user_checkbox">
                                        <label for="userCheckbox"></label>
                                    </span>
                                </td>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $row["FIRST_NAME"] . ' ' . $row["LAST_NAME"]; ?></td>
                                <td>
                                    <span class="custom-checkbox">
                                        <input type="checkbox" name="adminCheckbox" class="user_checkbox" data-user-id="<?php echo $row["USER_ID"]; ?>" <?php if ($row["ADMIN"]) echo "checked" ?>>
                                        <label for="adminCheckbox"></label>
                                    </span>
                                </td>
                                <td><?php echo $row["EMAIL"]; ?></td>
                                <td><?php if ($row["GENDER"] == 0) echo 'Male';
                                    elseif ($row["GENDER"] == 1) echo 'Female';
                                    else echo 'Other'
                                    ?>
                                </td>
                                <td>
                                    <a href="#editEmployeeModal" data-toggle="modal" class="editBTN" data-user-id="<?php echo $row['USER_ID']?>" title="Edit">
                                        <span class="edit glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="#deleteEmployeeModal" data-toggle="modal" data-user-id="<?php echo $row['USER_ID'] ?>" data-page="<?php echo $_GET['page-id'] ?>" title="Delete">
                                        <span class="delete glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php
                            $i++;
                        }
                        ?>
                        <input type="hidden" name="pageID" value="<?php echo $_GET['page'] ?>" class="form-control">
                    </tbody>
                </table>
            </form>
            <div id="notification">
            </div>
        </div>
        <?php if (isset($_SESSION['updateResult'])) : ?>
            <h2>Update database of <?php echo $_SESSION['updateResult'] ?> successfully</h2>
        <?php
            unset($_SESSION['updateResult']);
        endif; ?>

        <!-- <?php
                if (isset($_SESSION['delUser'])) : ?>
            <h2 class="text-success">Deleted</h2>
        <?php
                    unset($_SESSION['delUser']);
                endif; ?> -->
        <?php
        if (isset($_SESSION['addUser'])) : ?>
            <h2 class="text-success">Added</h2>
        <?php
            unset($_SESSION['addUser']);
        endif; ?>
        <!-- <ul class="pagination pull-right">
            <li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>
        </ul> -->
    </div>

    <!-- Add Modal HTML -->
    <div id="addEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="addUser.php" method="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Add User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="pageID" value="<?php echo $page ?>" />

                        <div class="form-group">
                            <label for="userName"><b>User Name</b></label>
                            <input type="text" placeholder="User Name" name="userName" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="firstName"><b>First Name</b></label>
                            <input type="text" placeholder="First Name" name="firstName" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="lastName"><b>Last Name</b></label>
                            <input type="text" placeholder="Last Name" name="lastName" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <label for="email"><b>Email</b></label>
                            <input type="text" placeholder="Enter Email" name="email" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <label for="psw"><b>Password</b></label>
                            <input type="password" placeholder="Enter Password" name="psw" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <label for=""><b>Gender</b></label> <br>
                            <label class="radio-inline">
                                <input type="radio" name="gender" checked value="0">Male
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="1">Female
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="2">Other
                            </label>
                        </div>
                        <div class="form-group">
                            <label for=""><b>Language</b></label> <br>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="1">Tiếng Việt
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="2">English
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="3">français
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="4">русский
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="5">中文
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="100">Other
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-add" name="addUser">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Modal HTML -->
    <div id="editEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="addUser.php" enctype="multipart/form-data" method="POST">
                    <div class="modal-header">
                        <h4 class="modal-title">Edit User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="pageID" value="<?php echo $_GET['page'] ?>" />
                        <input type="hidden" name="userID" value="">
                        <div class="form-group">
                            <label for="firstName"><b>First Name</b></label>
                            <input type="text" placeholder="First Name" name="firstName" class="form-control" required />
                        </div>
                        <div class="form-group">
                            <label for="lastName"><b>Last Name</b></label>
                            <input type="text" placeholder="Last Name" name="lastName" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <label for="email"><b>Email</b></label>
                            <input type="text" placeholder="Enter Email" name="email" class="form-control" required />
                        </div>

                        <div class="form-group">
                            <label for=""><b>Gender</b></label> <br>
                            <label class="radio-inline">
                                <input type="radio" name="gender" checked="checked" value="0">Male
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="1">Female
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="gender" value="2">Other
                            </label>
                        </div>
                        <div class="form-group">
                            <label for=""><b>Language</b></label> <br>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="1">Tiếng Việt
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="2">English
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="3">français
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="4">русский
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="5">中文
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="language[]" value="100">Other
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-success" id="btn-edit" name="editUser">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Del Modal HTML -->
    <div id="deleteEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form name="delUser">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="userID" name="userID" value="" class="form-control">
                        <input type="hidden" name="pageID" value="<?php echo $_GET['page'] ?>" class="form-control">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="BTNdelUser">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Del multiple Modal HTML -->
    <div id="deleteMultipleEmployeeModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" name="delUser">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to delete these Records?</p>
                        <p class="text-warning"><small>This action cannot be undone.</small></p>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <button type="button" class="btn btn-danger" id="delMulUser">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="../js/fillData.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
            var checkbox = $('table tbody td:first-child .user_checkbox');
            $("#selectAll").click(function() {
                if (this.checked) {
                    checkbox.each(function() {
                        this.checked = true;
                    });
                } else {
                    checkbox.each(function() {
                        this.checked = false;
                    });
                }
            });
            checkbox.click(function() {
                if (!this.checked) {
                    $("#selectAll").prop("checked", false);
                }
            });
        });

    </script>
    <script src="../js/fillData.js"></script>
    <script src="../ajax/delUser.js"></script>
    <script src="../ajax/addUser.js"></script>
    <script src="../ajax/editUser.js"></script>

</body>

</html>