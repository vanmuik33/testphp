<?php
$roleAdmin = array('FULL ACCESS');
include_once('../function.php');
include_once('../_permission.php');
$userID = checkLoginType();
checkPermission($roleAdmin);
// Các biến xử lý thông báo
$show_alert = '<script>$("#notification .alert").removeClass("hidden");</script>';
$hide_alert = '<script>$("#notification .alert").addClass("hidden");</script>';
$success = '<script>$("#notification .alert").attr("class", "alert alert-success");</script>';
include_once('../connect.php');
if (isset($_GET['type']) && $_GET['type'] == 'delPost') {
    $id_post = trim(htmlspecialchars(addslashes($_GET['postID'])));
    $sql_check_id_post_exist = "SELECT ID_POST FROM POSTS WHERE ID_POST = '$id_post'";
    if (mysqli_num_rows(mysqli_query($connect, $sql_check_id_post_exist))) {
        $sql_delete_post = "UPDATE `POSTS` SET `IS_DELETE` = 1 WHERE `ID_POST` = $id_post";
        mysqli_query($connect,$sql_delete_post);
        mysqli_close($connect);
        echo $success . 'Bài viết đã xoá thành công';
    }
    else {
        echo $show_alert . 'Bài viết này không tồn tại hoặc đã bị xoá.';
    }

}
