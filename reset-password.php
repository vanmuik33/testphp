<?php
include('connect.php');
$error = '';
if (
  isset($_GET["key"]) && isset($_GET["email"]) && isset($_GET["action"]) && ($_GET["action"] == "reset") && !isset($_POST["action"])
) {
  $key = $_GET["key"];
  $email = $_GET["email"];
  $curDate = date("Y-m-d H:i:s");
  $query = mysqli_query($connect, "SELECT * FROM `TOKEN_TEMP` WHERE `KEY`='" . $key . "' and `EMAIL`='" . $email . "';");
  $row = mysqli_num_rows($query);
  if ($row == "") {
    $error .= '<h2>Invalid Link</h2><p>The link is invalid/expired. Either you did not copy the correct link from the email, or you have already used the key in which case it is deactivated.</p><p><a href="/testphp/forgotpw.php">Click here</a> to reset password.</p>';
  } else {
    $row = mysqli_fetch_assoc($query);
    $expDate = $row['expDATE'];
    if ($expDate >= $curDate) {
?>
      <!DOCTYPE html>
      <html lang="en">

      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <?php include('bootstrap3.php'); ?>
      </head>

      <body>
        <?php include('navbar.php'); ?>
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
              <form method="post" action="" name="update">
                <br />
                <label><strong>Enter New Password:</strong></label><br />
                <input type="password" name="pass1" maxlength="15" required />
                <br /><br />
                <label><strong>Re-Enter New Password:</strong></label><br />
                <input type="password" name="pass2" maxlength="15" required />
                <br /><br />
                <input type="hidden" name="email" value="<?php echo $email; ?>" />
                <input class="btn btn-primary" type="button" value="Reset Password" name="reset" />
              </form>
              <div id="notification" class="text-danger">

              </div>
            </div>
          </div>
        </div>
        <script src="ajax/reset.js"></script>
      </body>

      </html>

<?php
    } else {
      $error .= "<h2>Link Expired</h2>
      <p>The link is expired. You are trying to use the expired link which as valid only 24 hours (1 days after request).<br /><br /></p>";
    }
  }
  if ($error != "") {
    echo "<div class='error'>" . $error . "</div><br />";
  }
} // isset email key validate end
?>

<?php
if (isset($_POST["type"]) && ($_POST["type"] == 'reset')) {
  $error = "";
  $pass1 = mysqli_real_escape_string($connect, $_POST["pass1"]);
  $pass2 = mysqli_real_escape_string($connect, $_POST["pass2"]);
  $regex = "/^(?=.+[A-Z]).{8}$/";


  $email = $_POST['email'];
  if ($pass1 != $pass2) {
    $error .= "<li>Password do not match, both password should be same.<br /><br /></li>";
  };
  if (!preg_match($regex, $pass1)) {
    $error .= '<li>Password tu 6 den 32 ky tu, can co 1 chu cai viet hoa</li>';
  }
  if ($error != "") {
    echo "<div class='error'>" . $error . "</div><br />";
    die();
  } else {
    $pass1 = md5($pass1);
    mysqli_query($connect, "UPDATE `USERS` SET `PASSWORD`='" . $pass1 . "'WHERE `EMAIL`='" . $email . "';");
    mysqli_query($connect, "DELETE FROM `TOKEN_TEMP` WHERE `EMAIL`='" . $email . "';");
    echo 1;
    die();
  }
};
?>