$(document).ready(function () {
    $('#formEditPost button').on('click', function () {
        $id_post = $('#formEditPost').attr('data-id');
        $stt_edit_post = $('#formEditPost input[name="stt_edit_post"]:radio:checked').val();
        $title_edit_post = $('#title_edit_post').val();
        $slug_edit_post = $('#slug_edit_post').val();
        $url_thumb_edit_post = $('#url_thumb_edit_post').val();
        $desc_edit_post = $('#desc_edit_post').val();
        $keywords_edit_post = $('#keywords_edit_post').val();
        $body_edit_post = CKEDITOR.instances['editor1'].getData();
        if ($stt_edit_post == '' || $title_edit_post == '' || $slug_edit_post == '' || $body_edit_post == '') {
            $('#formEditPost .alert').removeClass('hidden');
            $('#formEditPost .alert').html('Vui lòng điền đầy đủ thông tin.');
        }
        else {
            $.ajax({
                type: 'POST',
                data: {
                    id_post: $id_post,
                    stt_edit_post: $stt_edit_post,
                    title_edit_post: $title_edit_post,
                    slug_edit_post: $slug_edit_post,
                    url_thumb_edit_post: $url_thumb_edit_post,
                    keywords_edit_post: $keywords_edit_post,
                    desc_edit_post: $desc_edit_post,
                    body_edit_post: $body_edit_post,
                    action: 'edit_post'
                }, success: function (data) {
                    $('#formEditPost .alert').removeClass('hidden');
                    $('#formEditPost .alert').html(data);
                }, error: function () {
                    $('#formEditPost .alert').removeClass('hidden');
                    $('#formEditPost .alert').html('Đã có lỗi xảy ra, hãy thử lại sau.');
                }
            });
        }
    });
});