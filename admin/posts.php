<?php
$roleAdmin = array('READ','FULL ACCESS');
include_once('../connect.php');
include('../function.php');
include_once('../_permission.php');

$userID = checkLoginType();
checkPermission($roleAdmin);

$limit = 5;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 1;
}
$from = ($page - 1) * $limit;

if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == TRUE) {
    $getDB_SQL = "SELECT * FROM `POSTS` WHERE `IS_DELETE` != 1 ORDER BY `ID_POST` LIMIT $from , $limit";
} else {
    $getDB_SQL = "SELECT * FROM `POSTS` WHERE (`AUTHOR_ID` = $userID AND `IS_DELETE` != 1 ) ORDER BY `ID_POST` LIMIT $from , $limit";
}

$getDB = mysqli_query($connect, $getDB_SQL);

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List Post</title>
    <?php include('../bootstrap3.php') ?>
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/fillData.js" type="text/javascript"></script>
    <style>
        .text-white {
            color: #fff !important;
        }
    </style>
</head>

<body>
    <?php include('../navbar.php') ?>
    <div class="container">
        <p id="success"></p>
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h2>Manage <b>Posts</b></h2>
                    </div>
                    <div class="col-sm-6">
                        <a href="addPost.php" class="btn btn-success"><span class="glyphicon glyphicon-plus-sign"> </span><span> Add New Post</span></a>
                        <!-- <a href="#deleteMultipleEmployeeModal" class="btn btn-danger" id="delete_multiple" data-toggle="modal"><span class="glyphicon glyphicon-minus-sign"> </span> <span> Delete</span></a> -->
                        <!-- <a href="roles.php?page=1" class="btn btn-info" id="e"><span class="glyphicon glyphicon-check"> </span> <span> Roles & Permissions</span></a> -->
                    </div>
                </div>
            </div>
            <form action="" id="postForm">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <!-- <th>
                                <span class="custom-checkbox">
                                    <input type="checkbox" id="selectAll" name="selectAll">
                                    <label for="selectAll"></label>
                                </span>
                            </th> -->
                            <th>#</th>
                            <th>Author</th>
                            <th>TITLE</th>
                            <th>VIEW</th>
                            <th>STATUS</th>
                            <th>TOOLS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        while ($row = mysqli_fetch_array($getDB)) {
                            // Tác giả bài viết
                            $sql_get_author = "SELECT FIRST_NAME,LAST_NAME FROM USERS WHERE (`USER_ID` = ".$row['AUTHOR_ID']. " OR `USER_GG_ID` = ".$row['AUTHOR_ID'] . " OR `USER_FB_ID` = ".$row['AUTHOR_ID'] .")";
                            if (mysqli_num_rows(mysqli_query($connect, $sql_get_author))) {
                                $data_author = mysqli_fetch_assoc(mysqli_query($connect, $sql_get_author));
                                $author_post = $data_author['FIRST_NAME'] . ' ' . $data_author['LAST_NAME'];
                            } else {
                                $author_post = '<span class="text-danger">Lỗi</span>';
                            }
                        ?>
                            <tr id="<?php echo $row["ID_POST"]; ?>">
                                <!-- <td>
                                    <span class="custom-checkbox">
                                        <input type="checkbox" name="userCheckbox[]" value="<?php echo $row["ID_POST"]; ?>" class="user_checkbox">
                                        <label for="userCheckbox"></label>
                                    </span>
                                </td> -->
                                <td><?php echo $i; ?></td>
                                <td><?php echo $author_post ?></td>
                                <td><?php echo $row["TITLE"]; ?></td>
                                <td><?php echo $row["VIEW"]; ?></td>
                                <td><?php echo $row["STATUS"]; ?></td>
                                <td>
                                    <a href="editPost.php?idPost=<?php echo $row['ID_POST'] ?>" class="editBTN" data-user-id="<?php echo $row['ID_POST'] ?>" title="Edit">
                                        <span class="edit glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="#deleteEmployeeModal" data-toggle="modal" data-post-id="<?php echo $row['ID_POST'] ?>" data-page="<?php echo $_GET['page'] ?>" title="Delete">
                                        <span class="delete glyphicon glyphicon-trash"></span>
                                    </a>
                            </tr>
                        <?php
                            $i++;
                        }
                        ?>
                        <input type="hidden" name="pageID" value="<?php echo $_GET['page'] ?>" class="form-control">
                    </tbody>
                </table>
            </form>
            <div id="notification">
                <div class="alert alert-danger hidden"></div>
            </div>
            <div id="deleteEmployeeModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form name="delPost">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete User</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <input type="hidden" id="postID" name="postID" value="" class="form-control">
                                <input type="hidden" name="pageID" value="<?php echo $_GET['page'] ?>" class="form-control">
                                <p>Are you sure you want to delete these Records?</p>
                                <p class="text-warning"><small>This action cannot be undone.</small></p>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <button type="button" class="btn btn-danger" id="BTNdelPost">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
            $getDBcount = mysqli_query($connect, "SELECT COUNT(`ID_POST`) FROM `POSTS` WHERE (`IS_DELETE` !=1 AND `AUTHOR_ID` = $userID )");
            $DBcout = mysqli_fetch_row($getDBcount);
            $totalPost = $DBcout[0];
            $totalPage = ceil($totalPost / $limit);
            $pagLink = "<ul class='pagination pull-right'>";
            for ($i = 1; $i <= $totalPage; $i++) {
                if ($page == $i) $active = 'active';
                else $active = '';
                $pagLink .= "<li class='page-item " . $active . "'><a class='page-link' href='posts.php?page=" . $i . "'>" . $i . "</a></li>";
            }
            echo $pagLink . "</ul>";
            ?>
        </div>
        <script>
            $(document).ready(function() {
                var checkbox = $('table tbody td:first-child .user_checkbox');
                $("#selectAll").click(function() {
                    if (this.checked) {
                        checkbox.each(function() {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function() {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function() {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
                // Fill data to delete modal
                $('#deleteEmployeeModal').on('show.bs.modal', function(e) {
                    var postID = $(e.relatedTarget).data('post-id');
                    $('input[name="postID"]').attr('value', postID);
                });
            });
        </script>
        <!-- Liên kết thư viện CKEditor -->
        <script src="<?php echo $_DOMAIN; ?>ckeditor/ckeditor.js"></script>
        <script src="../ajax/delPost.js"></script>

</body>

</html>