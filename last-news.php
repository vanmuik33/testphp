<div class="row">
    <?php
    $_DOMAIN = 'https://muinv.lahvui.xyz/testphp/';

    while ($row = mysqli_fetch_assoc($DB)) { ?>
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <a href="<?php echo $_DOMAIN . $row['SLUG'] . '-' . $row['ID_POST'] . '.html' ?>"> <img class="thumb" src="<?php echo $row['THUMB'] ?>" alt="<?php echo $row['KEYWORDS'] ?>"></a>
                <div class="caption">
                    <h3><a href="<?php echo $_DOMAIN . 'display.php?idpost=' . $row['ID_POST'] ?>"><?php echo $row['TITLE'] ?></a></h3>
                    <?php echo htmlspecialchars_decode($row['DESCR']) ?>
                </div>
            </div>
        </div>
    <?php
    }
    ?>

</div>